\textbf{by Peter Hügel}\\
For the nearest neighbor method, the squared euclidean distance between two images over all specified features is calculated. Effectively calculating the distance in an $n$-dimensional space, $n$ being the number of dimensions of all features combined. This distance is calculated between all training and all validation images. A naive approach without using vectorization takes a lot of time. Implementing a vectorized version cuts down the time required dramatically. A distance matrix containing those distances is calculated and can then be sorted to extract the minimal $k$ training images for each validation image. The label occurring the most in those $k$ training images is chosen to be the prediction for the respective validation image. To make the calculation of the distance not be imbalanced and be dominated by one feature dimension, it is necessary to norm each dimension first. Scaling all of the selected features to values between 0.0 and 1.0 means dividing them by their maximum possible value. The shape approximation has to be divided by 10,000 because that is the number of pixels in each image. Since there exist 4096 bins in the histogram, the color diversity feature is divided by just that. For the 3 dimensional features each dimension has to be normed individually. The center pixel feature is described as a simple RGB value, of course ranging from 0 to 255 in all 3 dimensions. Red, green, and blue are scaled to be within the desired range. Finally the peak of the histogram is also scaled in each dimension. Having 16 bins along each axis means a division by 15 is necessary for it to lie within 0.0 and 1.0. After all feature dimension are normed, a weight is applied to scale the relevance of each feature as desired. For the 3 dimensional features the same weight is applied to all 3 dimensions. To calculate the squared euclidean distance $(x_i-y_j)^2$ with vectorization, the following matrices have to be constructed to compute the equivalence of $x_i^2 + y_j^2 - 2x_i y_j$.\\
\begin{equation}
x =
\begin{bmatrix}
x_{1,1} & x_{1,2} & \dots & x_{1,d}\\
x_{2,1} & x_{2,2} & \dots & x_{2,d}\\
\vdots & \vdots & \ddots & \vdots\\
x_{m,1} & x_{m,2} & \dots & x_{m,d}
\end{bmatrix}
\in \mathbb{R}^{m \times d}\\
\end{equation}
\begin{equation}
y =
\begin{bmatrix}
y_{1,1} & y_{1,2} & \dots & y_{1,d}\\
y_{2,1} & y_{2,2} & \dots & y_{2,d}\\
\vdots & \vdots & \ddots & \vdots\\
y_{n,1} & y_{n,2} & \dots & y_{n,d}
\end{bmatrix}
\in \mathbb{R}^{n \times d}\\
\end{equation}\\
$m$ being the number of training images. $n$ being the number of validation images. $d$ being the total number of dimensions for the current selection of features.\\
$x_{m,d}$ is the already normed and weighted feature $d$ of the training image $m$.\\
$y_{n,d}$ is the already normed and weighted feature $d$ of the validation image $n$.\\
\begin{equation}
XX =
\begin{bmatrix}
\sum_d{x_{1,d}} & \dots & \sum_d{x_{1,d}}\\
\sum_d{x_{2,d}} & \dots & \sum_d{x_{2,d}}\\
\vdots & \ddots & \vdots\\
\sum_d{x_{m,d}} & \dots & \sum_d{x_{m,d}}
\end{bmatrix}
\in \mathbb{R}^{m \times n}\\
\end{equation}
\begin{equation}
YY =
\begin{bmatrix}
\sum_d{y_{1,d}} & \sum_d{y_{2,d}} & \dots & \sum_d{y_{n,d}}\\
\vdots & \vdots & \ddots & \vdots\\
\sum_d{y_{1,d}} & \sum_d{y_{2,d}} & \dots & \sum_d{y_{n,d}}
\end{bmatrix}
\in \mathbb{R}^{m \times n}\\
\end{equation}
\begin{equation}
XY = 2xy^T =
\begin{bmatrix}
\sum_d{x_{1,d} y_{1,d}} & \sum_d{x_{1,d} y_{2,d}} & \dots & \sum_d{x_{1,d} y_{n,d}}\\
\sum_d{x_{2,d} y_{1,d}} & \sum_d{x_{2,d} y_{2,d}} & \dots & \sum_d{x_{2,d} y_{n,d}}\\
\vdots & \vdots & \ddots & \vdots\\
\sum_d{x_{m,d} y_{1,d}} & \sum_d{x_{m,d} y_{2,d}} & \dots & \sum_d{x_{m,d} y_{n,d}}\\
\end{bmatrix}
\in \mathbb{R}^{m \times n}\\
\end{equation}\\
Now the final distance matrix $D$ can be calculated:\\
\begin{equation}
\begin{aligned}
D &= XX + YY - 2XY\\
&=
\begin{bmatrix}
\sum_d{x_{1,d}+y_{1,d}-2x_{1,d} y_{1,d}} & \dots & \sum_d{x_{1,d}+y_{n,d}-2x_{1,d} y_{n,d}}\\
\vdots & \ddots & \vdots\\
\sum_d{x_{m,d}+y_{1,d}-2x_{m,d} y_{1,d}} & \dots & \sum_d{x_{m,d}+y_{n,d}-2x_{m,d} y_{n,d}}\\
\end{bmatrix}
\in \mathbb{R}^{m \times n} \\
\end{aligned}
\end{equation}\\
The indices of the $k$ lowest values in collumn $n$ of the distance matrix $D$ indicate the $k$ nearest neighbors of validation image $n$.