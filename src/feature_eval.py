import numpy as np
import os
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
from mpl_toolkits.mplot3d import Axes3D

#which fruit are we looking at?
# current_fruits = ["Banana"]
# current_fruits = ["Strawberry", "Plum", "Pitahaya Red"]
# current_fruits = ["Cactus fruit", "Lemon"]
# current_fruits = ["Lemon", "Kaki"]
current_fruits = ["Cherry", "Pineapple"]
# current_fruits = ["Maracuja", "Lemon", "Quince"]
# current_fruits = ["Grapefruit Pink", "Orange", "Tangelo"]
# current_fruits = ["Huckleberry", "Pitahaya Red", "Apple Granny Smith"]
# current_fruits = ["Huckleberry", "Apple Granny Smith", "Strawberry"]
# current_fruits = ["Banana", "Apple Braeburn"]
# current_fruits = ["Banana", "Apple Braeburn", "Peach", "Lemon", "Limes"]
# current_fruits = ["Apple Braeburn", "Apple Red 2", "Apple Red Delicious"]
# current_fruits = ["Raspberry", "Tamarillo", "Avocado"]
colors = ['r', 'g', 'b', 'k', 'y']
markers = ['o', 's', '^', 'D', 'x']
black_markers = np.empty(len(current_fruits), dtype=object)
for i, m, c_f in zip(range(len(current_fruits)), markers[:len(current_fruits)], current_fruits):
	black_markers[i] = mlines.Line2D([], [], color='black', marker=m, markersize=10, label=c_f)
print_settings = True




if print_settings:
	automatic_scaling = False
	bin_size = 100
	#histogram borders for 1d features
	ranges = [[0, 10000], [0, 600]]
	#limits for 3d plot
	ranges_3d = [[0, 15], [0, 15]]
else:
	automatic_scaling = True
	bin_size = 50
	#histogram borders for 1d features
	ranges = [[0, 10000], [0, 4096]]
	#limits for 3d plot
	ranges_3d = [[0, 15], [0, 15]]


def gethisto(cur_dir, imgname):
	face = misc.imread(os.fsdecode(cur_dir) + "/" + os.fsdecode(imgname))
	face = np.reshape(face, [np.shape(face)[0] * np.shape(face)[1], 3])

	#3d histogram
	return np.histogramdd(face, bins)

if 'feature_names' not in locals():
	feature_names = np.load("./feature_data/feature_names.npy")
	features = np.load("./feature_data/features.npy")
	features_3d = np.load("./feature_data/features_3d.npy")
	labels = np.load("./feature_data/labels.npy")
	imagenames = np.load("./feature_data/imagenames.npy")

feature_count = [int(len(features) / len(labels)), int(len(features_3d) / 3 / len(labels))]

features_3d = np.reshape(features_3d, (feature_count[1], len(labels)*3))
features = np.reshape(features, (feature_count[0], len(labels)))

#plot a histogram of every 1 dimensional feature
for f_index in range(feature_count[0]):
	for i, c_f in zip(range(len(current_fruits)), current_fruits):
		plt.figure(feature_names[f_index])
		if automatic_scaling:
			plt.hist([features[f_index][np.where(labels == c_f)]], bins=bin_size, label=c_f, alpha=0.5, color=colors[i])
		else:
			plt.hist([features[f_index][np.where(labels == c_f)]], bins=bin_size, range=(ranges[f_index][0], ranges[f_index][1]), label=c_f, alpha=0.5, color=colors[i])
	plt.title("histogram of " + feature_names[f_index])
	plt.ylabel("count")
	plt.xlabel(feature_names[f_index] + " value")
	plt.legend(loc='upper right')



features_3d[1] = features_3d[1] / 16

#plot a 3 dimensional diagram of cluster centers for 3 dimensional features
figs = np.empty(feature_count[1], dtype=object)
for f_index in range(feature_count[1]):
	figs[f_index] = plt.figure(feature_names[feature_count[0]+f_index])
	ax = figs[f_index].add_subplot(111, projection='3d')
	for i, c_f in zip(range(len(current_fruits)), current_fruits):
		feat = np.reshape(features_3d[f_index], (len(labels), 3))[np.where(labels == c_f)]
		if f_index == 1:
			feat = np.array(feat, dtype=int)

		unique, count = np.unique(feat, return_counts=True, axis=0)
		count = count / np.shape(feat)[0] * 1000

		for u, co in zip(unique, count):
			ax.scatter(u[0], u[1], u[2], c=(u[0]/15.0, u[1]/15.0, u[2]/15.0), marker=markers[i], label=c_f, s=co)

	plt.title("locations of " + feature_names[feature_count[0] + f_index])

	plt.xlim(ranges_3d[f_index][0], ranges_3d[f_index][1])
	plt.ylim(ranges_3d[f_index][0], ranges_3d[f_index][1])
	ax.set_zbound(ranges_3d[f_index][0], ranges_3d[f_index][1])

	ax.set_xlabel('R')
	ax.set_ylabel('G')
	ax.set_zlabel('B')
	
	# plt.legend(handles=[black_markers[0], black_markers[1], black_markers[2]], loc=(0.85,0.9))
	plt.legend(handles=black_markers.tolist(), loc=(0.85,0.9))

plt.show()