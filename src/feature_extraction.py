#!/usr/bin/env python

import numpy as np
from scipy import misc
import os
import sys

#number of bins
length = 16
width = 16
height = 16
bins = [length, width, height]

def readimage(cur_dir, imgname):
	return misc.imread(os.fsdecode(cur_dir) + "/" + os.fsdecode(imgname))


def gethisto(face, bins):
	face = np.reshape(face, [np.shape(face)[0] * np.shape(face)[1], 3])

	#3d histogram
	return np.histogramdd(face, bins)

def getspike(hist):
	#extract 3d coordinate of the histogram spike
	max_index = np.argsort(hist[0].flatten())[-1]
	B = (max_index % (length * width)) % width
	G = int((max_index % (length * width)) / width)
	R = int(max_index / (length * width))

	#filter all grey bins and bins that are within R < 3, B < 3, G < 3
	for x in range(16 + 60):
		if (R == G and G == B) or (R < 4 and G < 4 and B < 4):
			max_index = np.argsort(hist[0].flatten())[-2-x]
			B = (max_index % (length * width)) % width
			G = int((max_index % (length * width)) / width)
			R = int(max_index / (length * width))
		else:
			continue

	return np.array([R, G, B])

def getminshape(hist, face):
	face = np.reshape(face, [np.shape(face)[0] * np.shape(face)[1], 3])
	count = 0
	for f in face:
		if f[0] == 255 and f[1] == 255 and f[2] == 255:
			count += 1

	return count
	# return hist[0].flatten()[-1]

def getcolordiv(hist):
	return np.count_nonzero(hist[0].flatten())

#extract 3dimensional features from all of our data
#return features, labels, and imagenames (for debugging)
def extractdata():
	directory_one = "./fruits-360/Training"
	directory_two = "./fruits-360/Validation"

	dir_training = os.fsencode(directory_one)

	#final feature array containing all different features
	features = np.array([])
	features_3d = np.array([])

	#different features that we extract
	feature_names = np.array(["shape approximation", "color diversity", "RGB histogram spike", "RGB of center pixel"], dtype = str)
	minimalist_shape = np.array([])
	color_diversity = np.array([])
	histogram_3d = np.array([])
	center_pixel = np.array([])

	last_progress = 0.0
	#labels and imagenames that we extract
	labels = np.array([], dtype = object)
	imagenames = np.array([], dtype = object)
	for i, foldername in zip(range(len(os.listdir(dir_training))), os.listdir(dir_training)):
		last_progress = int(i / len(os.listdir(dir_training)) * 100)
		sys.stdout.write("extraction progress: %d%%   \r" % (last_progress))
		sys.stdout.flush()

		#filter for certain fruits for debugging purposes
		skip = False
		# fruit_filter = ["Apple Braeburn", "Avocado ripe", "Cherry", "Grapefruit Pink", "Peach", "Apple Red 1", "Apple Red 2", "Banana"]
		# fruit_filter = ["Avocado", "Apple Red 2"]
		# fruit_filter = ["Banana", "Apple Braeburn"]
		fruit_filter = ["Banana"]
		# fruit_filter = ["Cherry", "Plum"]
		for ff in fruit_filter:
			if os.fsdecode(foldername) == ff:
				skip = False
		if skip:
			continue

		#prepare temporary arrays of the current fruit
		dir_training_fruit = os.fsencode(directory_one+"/"+os.fsdecode(foldername))
		dir_validation_fruit = os.fsencode(directory_two+"/"+os.fsdecode(foldername))

		#1d
		minimalist_shape_tmp = np.empty(len(os.listdir(dir_training_fruit)) + len(os.listdir(dir_validation_fruit)))
		color_diversity_tmp = np.empty(len(os.listdir(dir_training_fruit)) + len(os.listdir(dir_validation_fruit)))
		#3d
		histogram_3d_tmp = np.empty([len(os.listdir(dir_training_fruit)) + len(os.listdir(dir_validation_fruit)), 3])
		center_pixel_tmp = np.empty([len(os.listdir(dir_training_fruit)) + len(os.listdir(dir_validation_fruit)), 3])

		labels_tmp = np.empty(len(os.listdir(dir_training_fruit)) + len(os.listdir(dir_validation_fruit)), dtype = object)
		imagenames_tmp = np.empty(len(os.listdir(dir_training_fruit)) + len(os.listdir(dir_validation_fruit)), dtype = object)

		#go through all training pictures and extract data
		for imagename, i in zip(os.listdir(dir_training_fruit), range(len(os.listdir(dir_training_fruit)))):
			
			labels_tmp[i] = os.fsdecode(foldername)
			imagenames_tmp[i] = imagename
			face = readimage(dir_training_fruit, imagename)
			hist = gethisto(face, bins)
			#the amount of pixels that are located in the bin that contains the whitest pixels
			minimalist_shape_tmp[i] = getminshape(hist, face)
			#the amount of bins containing at least one pixel
			color_diversity_tmp[i] = getcolordiv(hist)
			#extract the spike in the 3d histogram while ignoring spikes in the grey zones
			histogram_3d_tmp[i] = getspike(hist)
			#extract the RGB values of the center pixel of the image
			center_pixel_tmp[i] = face[50][50]

		#go through all validation pictures and extract data
		for imagename, i in zip(os.listdir(dir_validation_fruit), range(len(os.listdir(dir_training_fruit)), len(os.listdir(dir_validation_fruit)) + len(os.listdir(dir_training_fruit)))):
			
			labels_tmp[i] = os.fsdecode(foldername)
			imagenames_tmp[i] = imagename
			face = readimage(dir_validation_fruit, imagename)
			hist = gethisto(face, bins)
			#the amount of pixels that are located in the bin that contains the whitest pixels
			minimalist_shape_tmp[i] = getminshape(hist, face)
			#the amount of bins containing at least one pixel
			color_diversity_tmp[i] = getcolordiv(hist)
			#extract the spike in the 3d histogram while ignoring spikes in the grey zones
			histogram_3d_tmp[i] = getspike(hist)
			#extract the RGB values of the center pixel of the image
			center_pixel_tmp[i] = face[50][50]

		#add the information for this type of fruit to the final data-arrays
		labels = np.append(labels, labels_tmp)
		imagenames = np.append(imagenames, imagenames_tmp)

		minimalist_shape = np.append(minimalist_shape, minimalist_shape_tmp)
		color_diversity = np.append(color_diversity, color_diversity_tmp)
		histogram_3d = np.append(histogram_3d, histogram_3d_tmp)
		center_pixel = np.append(center_pixel, center_pixel_tmp)

	features = np.append(features, minimalist_shape)
	features = np.append(features, color_diversity)
	features_3d = np.append(features_3d, histogram_3d)
	features_3d = np.append(features_3d, center_pixel)

	print("extraction progress: 100%   ")
	return feature_names, features, features_3d, labels, imagenames


feature_names, features, features_3d, labels, imagenames = extractdata()

np.save("./feature_data/feature_names", feature_names)
np.save("./feature_data/features", features)
np.save("./feature_data/features_3d", features_3d)
np.save("./feature_data/labels", labels)
np.save("./feature_data/imagenames", imagenames)