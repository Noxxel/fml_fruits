import classification
import numpy as np
import os
import importlib

algo_count = 1
multi_runs = 5
print_individual_fruits = True
print_missclassifications = True
k = 5
group_apples = False
feature_norms = [10000, 4096, 15, 255]

if 'feature_names' not in locals():
	feature_names = np.load("./feature_data/feature_names.npy")
	features = np.load("./feature_data/features.npy")
	features_3d = np.load("./feature_data/features_3d.npy")
	labels = np.load("./feature_data/labels.npy")
	imagenames = np.load("./feature_data/imagenames.npy")
	feature_count = [int(len(features) / len(labels)), int(len(features_3d) / 3 / len(labels))]
	features_3d = np.reshape(features_3d, (feature_count[1], len(labels)*3))
	features = np.reshape(features, (feature_count[0], len(labels)))
else:
	importlib.reload(classification)

is_test = classification.split_data(len(labels))
test_labels = labels[np.where(is_test == 1)]

prediction_methods = np.full((feature_count[0] + feature_count[1]) * algo_count + multi_runs, "empty", dtype=object)
predictions = np.empty(((feature_count[0] + feature_count[1]) * algo_count + multi_runs) * len(test_labels), dtype=object)

#run every 1d feature through every algorithm
for f_index in range(feature_count[0]):
	#filter what to calculate
	filter_features = False
	if not filter_features or f_index == -1:
		replace_zone = np.arange(f_index * len(test_labels), (f_index+1) * len(test_labels))

		# np.put(predictions, replace_zone, classification.nn(features[f_index], labels, is_test))
		output_str_tmp = str(k)+"nn with 1d feature "+feature_names[f_index]
		print(output_str_tmp)
		np.put(prediction_methods, f_index, output_str_tmp)
		np.put(predictions, replace_zone, classification.knn_weighted(features[f_index], np.array([]), labels, is_test, [1], [1], k))

#run every 3d feature through every algorithm
for f_index in range(feature_count[1]):
	#filter what to calculate
	filter_features = False
	if not filter_features or f_index == -1:
		replace_zone = np.arange(feature_count[0] * len(test_labels) + (f_index * len(test_labels)),
		                         feature_count[0] * len(test_labels) + (f_index+1) * len(test_labels))

		# np.put(predictions, replace_zone, classification.nn(features_3d[f_index], labels, is_test))
		output_str_tmp = str(k)+"nn with 3d feature "+feature_names[feature_count[0] + f_index]
		print(output_str_tmp)
		np.put(prediction_methods, feature_count[0] + f_index, output_str_tmp)
		np.put(predictions, replace_zone, classification.knn_weighted(np.array([]), features_3d[f_index], labels, is_test, [1], [1], k))

# run the combination of features with weighting through all algorithms
run = 0
if True:
	replace_zone = np.arange((feature_count[0] + feature_count[1] + run) * len(test_labels),
	                         (feature_count[0] + feature_count[1] + run + 1) * len(test_labels))

	weights = [0.15, 0.85]
	output_str_tmp = (str(k)+"nn with " + feature_names[0] + " * " + str(weights[0]) +
	                  " and " + feature_names[feature_count[0]+0] + " * " + str(weights[1]))
	print(output_str_tmp)
	np.put(prediction_methods, feature_count[0] + feature_count[1] + run, output_str_tmp)
	np.put(predictions, replace_zone, classification.knn_weighted(features[0], features_3d[0], labels,
	                                                              is_test, weights, [feature_norms[0], feature_norms[2]], k))
run += 1
if False:
	replace_zone = np.arange((feature_count[0] + feature_count[1] + run) * len(test_labels),
	                         (feature_count[0] + feature_count[1] + run + 1) * len(test_labels))

	weights = [0.15, 0.85]
	output_str_tmp = (str(k)+"nn with " + feature_names[1] + " * " + str(weights[0]) +
	                  " and " + feature_names[feature_count[0]+0] + " * " + str(weights[1]))
	print(output_str_tmp)
	np.put(prediction_methods, feature_count[0] + feature_count[1] + run, output_str_tmp)
	np.put(predictions, replace_zone, classification.knn_weighted(features[1], features_3d[0], labels,
	                                                              is_test, weights, [feature_norms[1], feature_norms[2]], k))
run += 1
if False:
	replace_zone = np.arange((feature_count[0] + feature_count[1] + run) * len(test_labels),
	                         (feature_count[0] + feature_count[1] + run + 1) * len(test_labels))

	weights = [0.15, 0.85]
	output_str_tmp = (str(k)+"nn with " + feature_names[feature_count[0]+1] + " * " + str(weights[0]) +
	                  " and " + feature_names[feature_count[0]+0] + " * " + str(weights[1]))
	print(output_str_tmp)
	np.put(prediction_methods, feature_count[0] + feature_count[1] + run, output_str_tmp)
	np.put(predictions, replace_zone, classification.knn_weighted([], [features_3d[0], features_3d[1]], labels,
	                                                              is_test, weights, [feature_norms[2], feature_norms[3]], k))
run += 1
if False:
	replace_zone = np.arange((feature_count[0] + feature_count[1] + run) * len(test_labels),
	                         (feature_count[0] + feature_count[1] + run + 1) * len(test_labels))

	weights = [0.1, 0.3, 0.6]
	output_str_tmp = (str(k)+"nn with " + feature_names[0] + " * " + str(weights[0]) +
	                  " and " + feature_names[1] + " * " + str(weights[1]) +
	                  " and " + feature_names[feature_count[0]+0] + " * " + str(weights[2]))
	print(output_str_tmp)
	np.put(prediction_methods, feature_count[0] + feature_count[1] + run, output_str_tmp)
	np.put(predictions, replace_zone, classification.knn_weighted([features[0], features[1]], features_3d[0], labels,
	                                                              is_test, weights, [feature_norms[0], feature_norms[1], feature_norms[2]], k))
run += 1
if True:
	replace_zone = np.arange((feature_count[0] + feature_count[1] + run) * len(test_labels),
	                         (feature_count[0] + feature_count[1] + run + 1) * len(test_labels))

	weights = [0.12, 0.38, 0.46, 0.04]
	output_str_tmp = (str(k)+"nn with " + feature_names[0] + " * " + str(weights[0]) +
	                  " and " + feature_names[1] + " * " + str(weights[1]) +
	                  " and " + feature_names[feature_count[0]+0] + " * " + str(weights[2]) +
	                  " and " + feature_names[feature_count[0]+1] + " * " + str(weights[3]))
	print(output_str_tmp)
	np.put(prediction_methods, feature_count[0] + feature_count[1] + run, output_str_tmp)
	np.put(predictions, replace_zone, classification.knn_weighted([features[0], features[1]], [features_3d[0], features_3d[1]], labels,
	                                                              is_test, weights, [feature_norms[0], feature_norms[1], feature_norms[2], feature_norms[3]], k))

predictions = np.reshape(predictions, ((feature_count[0] + feature_count[1]) * algo_count + multi_runs, len(test_labels)))


for pred, pred_m in zip(predictions, prediction_methods):
	if str(pred_m) != "empty":
		print("")
		print(str(pred_m) + ":")
		total = 0
		total_c = 0
		count = 0
		correct = 0
		current_fruit = str(test_labels[0])
		missclassification = np.array([])
		for p, l, index in zip(pred, test_labels, range(len(test_labels))):
			total += 1
			count += 1
			if str(p) == str(l) or (np.core.defchararray.find(str(p), "Apple") != -1 and np.core.defchararray.find(str(l), "Apple") != -1 and group_apples):
				total_c += 1
				correct += 1
			else:
				missclassification = np.append(missclassification, str(p))
			if str(l) != current_fruit:
				if print_individual_fruits:
					print("success rate " + str(current_fruit) + ": " + str(correct / count * 100) + "%")
				if print_missclassifications and len(missclassification) > 0:
					print("missclassifications for "+str(current_fruit)+": ")
					print(missclassification)
				count = 0
				correct = 0
				current_fruit = str(l)
				missclassification = np.array([])

		if print_individual_fruits:
			print("success rate " + str(current_fruit) + ": " + str(correct / count * 100) + "%")
		if print_missclassifications and len(missclassification) > 0:
			print("missclassifications for "+str(current_fruit)+": ")
			print(missclassification)

		print("")
		print("success rate total: " + str(total_c / total * 100) + "%")

np.save("./results/prediction_methods", prediction_methods)
np.save("./results/predictions", predictions)
np.save("./results/test_labels", test_labels)