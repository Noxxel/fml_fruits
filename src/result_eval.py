import numpy as np
import os

load_dir = "final"
print_individual_fruits = False
print_missclassifications = True

predictions = np.load("./results/"+load_dir+"/predictions.npy")
prediction_methods = np.load("./results/"+load_dir+"/prediction_methods.npy")
test_labels = np.load("./results/"+load_dir+"/test_labels.npy")


def print_stuff(correct, count, pred_m, missclassification, current_fruit):
	if correct/count < 0.8 and pred_m == "knn with multiple features" or True:
		if print_individual_fruits:
			print(str(current_fruit) + ": " + str(correct / count * 100) + "%")
		if print_missclassifications and len(missclassification) > 0:
			print("missclassifications for "+str(current_fruit)+": ")
			print(missclassification)


for pred, pred_m in zip(predictions, prediction_methods):
	if pred_m != "empty":
		print("")
		print(str(pred_m) + ":")
		total = 0
		total_c = 0
		count = 0
		correct = 0
		current_fruit = str(test_labels[0])
		missclassification = np.array([])
		for p, l, index in zip(pred, test_labels, range(len(test_labels))):
			count += 1
			total += 1
			if str(p) == str(l):
				total_c += 1
				correct += 1
			else:
				missclassification = np.append(missclassification, str(p))
			if str(l) != current_fruit:
				print_stuff(correct, count, pred_m, missclassification, current_fruit)
				count = 0
				correct = 0
				current_fruit = str(l)
				missclassification = np.array([])

		print_stuff(correct, count, pred_m, missclassification, current_fruit)

		print("success rate total: " + str(total_c / total * 100) + "%")
