#!/usr/bin/env python

import numpy as np
from sklearn.model_selection import train_test_split
import sys

def split_data(data_size, test_size = 0.3):
	is_test = np.zeros(data_size)
	for x in range(int(data_size*test_size)):
		is_test[x] = 1

	np.random.seed(seed = 123456)
	np.random.shuffle(is_test)

	return is_test

#knn that can handle multiple features while norming and weighting them.
#if k is set to '-1',the mean of the training features is calculated and nearest mean is used instead.
def knn_weighted(features, features_3d, labels, is_test, weights, norm_values, k=1):
	#how many 1d features do we have?
	features_count = int(len(np.array(features).flatten()) / len(labels))
	#how many 3d features do we have?
	features_3d_count = int(len(np.array(features_3d).flatten()) / 3 / len(labels))

	assert(features_count + features_3d_count == len(weights))
	assert(len(weights) == len(norm_values))

	train_labels = labels[np.where(is_test == 0)]
	test_labels = labels[np.where(is_test == 1)]

	predictions = np.empty(len(test_labels), dtype=object)

	features = np.reshape(features, (features_count, len(labels)))
	features_3d = np.reshape(features_3d, (features_3d_count, len(labels) * 3))

	train_features = np.empty(len(train_labels) * features_count)
	test_features = np.empty(len(test_labels) * features_count)
	for (i, feat) in zip(range(features_count), features):
		replace_zone = np.arange(i * len(train_labels), (i+1) * len(train_labels))
		np.put(train_features, replace_zone, feat[np.where(is_test == 0)])
		replace_zone = np.arange(i * len(test_labels), (i+1) * len(test_labels))
		np.put(test_features, replace_zone, feat[np.where(is_test == 1)])

	train_features_3d = np.empty(len(train_labels) * features_3d_count * 3)
	test_features_3d = np.empty(len(test_labels) * features_3d_count * 3)
	for (i, feat) in zip(range(features_3d_count), features_3d):
		replace_zone = np.arange(i * len(train_labels) * 3, (i+1) * len(train_labels) * 3)
		np.put(train_features_3d, replace_zone, np.reshape(feat, (len(labels), 3))[np.where(is_test == 0)])
		replace_zone = np.arange(i * len(test_labels) * 3, (i+1) * len(test_labels) * 3)
		np.put(test_features_3d, replace_zone, np.reshape(feat, (len(labels), 3))[np.where(is_test == 1)])

	train_features = np.reshape(train_features, (features_count, len(train_labels)))
	test_features = np.reshape(test_features, (features_count, len(test_labels)))
	train_features_3d = np.reshape(train_features_3d, (features_3d_count, len(train_labels)*3))
	test_features_3d = np.reshape(test_features_3d, (features_3d_count, len(test_labels)*3))

	unique_labels = np.unique(train_labels)

	x = []
	y = []
	for i, tr_feat, te_feat in zip(range(features_count), train_features, test_features):
		if k > 0:
			x.append((tr_feat / norm_values[i] * weights[i]).tolist())
			y.append((te_feat / norm_values[i] * weights[i]).tolist())
		else:
			tmp_mean = np.array([])
			for u_l in unique_labels:
				tmp_mean = np.append(tmp_mean,
				                     (np.sum(tr_feat[np.where(train_labels == u_l)])
				                             / len(tr_feat[np.where(train_labels == u_l)])
				                             / norm_values[i] * weights[i]).tolist())
			x.append(tmp_mean)
			y.append((te_feat / norm_values[i] * weights[i]).tolist())
		
	for i, tr_feat, te_feat in zip(range(features_3d_count), train_features_3d, test_features_3d):
		if k > 0:
			for j in range(3):
				x.append((np.reshape(tr_feat, (len(train_labels), 3))[:,j]
				                     / norm_values[features_count+i] * weights[features_count+i]).tolist())
				y.append((np.reshape(te_feat, (len(test_labels), 3))[:,j]
				                     / norm_values[features_count+i] * weights[features_count+i]).tolist())
		else:
			for j in range(3):
				tmp_mean = np.array([])
				for u_l in unique_labels:
					tmp_mean = np.append(tmp_mean, (np.sum(np.reshape(tr_feat, (len(train_labels), 3))[np.where(train_labels == u_l)][:,j]) /
						                len(np.reshape(tr_feat, (len(train_labels), 3))[np.where(train_labels == u_l)][:,j])
						                / norm_values[features_count+i] * weights[features_count+i]).tolist())
				x.append(tmp_mean)
				y.append((np.reshape(te_feat, (len(test_labels), 3))[:,j] / norm_values[features_count+i] * weights[features_count+i]).tolist())

	m = len(train_labels)
	n = len(test_labels)

	x = np.stack(x).T
	y = np.stack(y).T

	xx = np.stack([(x**2).sum(axis=1)] * n, axis=0)
	if k > 0:
		yy = np.stack([(y**2).sum(axis=1)] * m, axis=1)
	else:
		yy = np.stack([(y**2).sum(axis=1)] * len(unique_labels), axis=1)
	xy = np.dot(x, y.T)
	
	dist_mat = xx + yy - 2*xy.T

	if not k > 0:
		k = 1
		sorted_indices = np.argsort(dist_mat, axis=1)[:,:k]

		for i, s_i in enumerate(sorted_indices):
			predictions[i] = unique_labels[s_i[0]]
	else:
		sorted_indices = np.argsort(dist_mat, axis=1)[:,:k]

		for i, s_i in enumerate(sorted_indices):
			#return_counts=True is needed for majority vote
			# unique, count = np.unique(train_labels[s_i], return_counts=True)
			unique = np.unique(train_labels[s_i], return_counts=False)

			#if weighted vote:
			d_1 = dist_mat[i][s_i[0]]
			d_k = dist_mat[i][s_i[-1]]
			distance_based_weights = np.empty(len(unique), dtype=float)
			for counter, u in enumerate(unique):
				distances_current_fruit = dist_mat[i][s_i[np.where(u == train_labels[s_i])]]
				#make sure we don't devide by 0:
				if u == train_labels[s_i[0]]:
					distances_current_fruit = distances_current_fruit[1:]
					distance_based_weights[counter] = 1
				else:
					distance_based_weights[counter] = 0
				distances_current_fruit = (d_k + (distances_current_fruit * -1)) / (d_k - d_1)
				distance_based_weights[counter] += np.sum(distances_current_fruit)

			predictions[i] = unique[np.argsort(distance_based_weights)[-1]]

			#if majority vote:
			# predictions[i] = unique[np.argsort(count)[-1]]


	return predictions